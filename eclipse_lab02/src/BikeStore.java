
public class BikeStore {
	public static void main(String[] args) {
		Bicycle[] mySetOfBikes = new Bicycle[4];
		mySetOfBikes[0] = new Bicycle("Schwin", 4, 30.6);
		mySetOfBikes[1] = new Bicycle("Shimano", 30, 253.9);
		mySetOfBikes[2] = new Bicycle("Cannondale", -3, -45.6);
		mySetOfBikes[3] = new Bicycle("Scott", 5, 40);

		
		for(int i = 0; i<mySetOfBikes.length; i++) {
			System.out.println(mySetOfBikes[i]);
		}
	}
	
}
