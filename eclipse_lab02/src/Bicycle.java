// Juan-Carlos Sreng-Flores 
// ID: 1533920
public class Bicycle {
	private String manufacturer;
	private int numberOfGears;
	private double maxSpeed;
	public Bicycle(String manufacturer, int gearsNumber, double maximumSpeed) {
		this.manufacturer = manufacturer;
		numberOfGears = gearsNumber;
		maxSpeed = maximumSpeed;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public int getNumberofGears() {
		return numberOfGears;
	}
	public double getMaxSpeed() {
		return maxSpeed;
	}
	public String toString() {
		return "Manufacturer: "+manufacturer+", Number of gears: "+numberOfGears+", Maximum Speed: "+maxSpeed+".";
	}
}
